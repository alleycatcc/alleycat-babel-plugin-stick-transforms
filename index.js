const LEGACY_CALL = "defineBinaryOperator"

// --- compile away legacy calls of `defineBinaryOperator (...)`, which are no longer necessary.

const isLegacyDefinition = (t, node) => {
  const { expression={}, } = node
  const { callee={}, } = expression
  const { name={}, } = callee

  return true
    && t.isCallExpression (expression)
    && t.isIdentifier (callee)
    && name === LEGACY_CALL
}

const o = {
  '|': 'pipe',
  '>>': 'composeRight',
  '<<': 'compose',
}

module.exports = ({ types: t, }) => ({
  visitor: {
    ExpressionStatement: (path) => {
      const { node, } = path
      if (!isLegacyDefinition (t, node)) return
      path.replaceWith (t.noop ())
    },
    BinaryExpression: (path) => {
      const { node, } = path
      const { operator, } = node
      const replaceName = o [operator]
      if (!replaceName) return

      path.replaceWith (t.CallExpression (
        { type: 'Identifier', name: replaceName, },
        [node.left, node.right]),
      )
    },
  },
})
